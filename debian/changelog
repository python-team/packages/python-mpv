python-mpv (1.0.7-2) unstable; urgency=medium

  * d/patches: add 0006 to skip flaky test. (Closes: #1076971)

 -- Louis-Philippe Véronneau <pollo@debian.org>  Sat, 24 Aug 2024 00:44:48 -0400

python-mpv (1.0.7-1) unstable; urgency=medium

  * New upstream release.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Wed, 03 Jul 2024 20:16:16 -0400

python-mpv (1.0.6-1) unstable; urgency=medium

  * New upstream release. (Closes: #1070252)

 -- Louis-Philippe Véronneau <pollo@debian.org>  Fri, 03 May 2024 11:16:57 -0400

python-mpv (1.0.4-1) unstable; urgency=medium

  * New upstream release.
  * d/control: replace xvfbwrapper by pyvirtualdisplay.
  * d/tests: remove in favor of pybuild-autopkgtest.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Tue, 25 Jul 2023 12:08:32 -0400

python-mpv (1.0.3-1) unstable; urgency=medium

  * New upstream release.
  * d/patches: drop 0005, as it has been upstreamed. Also drop 0004, as it is
    not needed anymore.
  * d/control: Standards-Version update to 4.6.2. No changes needed.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Mon, 12 Jun 2023 14:09:43 -0400

python-mpv (1.0.1-3) unstable; urgency=medium

  * d/control: depend on libmpv2. (Closes: #1024900)
  * d/tests/unittests: use py3versions -s instead of -s.
  * d/control: Standards-Version update to 4.6.1. No changes.
  * d/control, d/patches/0005: migrate to pyproject.toml.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Sun, 27 Nov 2022 19:50:50 -0500

python-mpv (1.0.1-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 26 May 2022 19:14:27 +0100

python-mpv (1.0.1-1) unstable; urgency=medium

  * New upstream release.
  * d/copyright: now licensed under GPL-2+ or LGPL2.1+.
  * d/patches: drop 0003, as it has been upstreamed.
  * d/patches: drop 0002 (fixed) and add 0004 (new testsuite failures).

 -- Louis-Philippe Véronneau <pollo@debian.org>  Sun, 24 Apr 2022 23:06:23 -0400

python-mpv (0.5.2-3) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Commit the Ubuntu delta into Debian git repository.

  [ Paride Legovini ]
  * d/patches/test_property_observer_decorator-bump-sleep.patch
    Fixes flaky test with mpv 0.33.1 on ppc64el. (LP: #1945332)
    (Closes: #1000807)

  [ Louis-Philippe Véronneau ]
  * d/patches: rename test_property_observer_decorator-bump-sleep.patch to
    0003, to be consistent with existing naming scheme.
  * d/control: Standards-Version update to 4.6.0. No changes.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Mon, 29 Nov 2021 12:08:56 -0500

python-mpv (0.5.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Louis-Philippe Véronneau ]
  * d/patches: add 0002 to patch out broken test. (Closes: #1000255)

 -- Louis-Philippe Véronneau <pollo@debian.org>  Sat, 27 Nov 2021 14:14:35 -0500

python-mpv (0.5.2-1) unstable; urgency=medium

  [ Louis-Philippe Véronneau ]
  * New upstream release.
  * d/control: use debhelper 13.
  * d/control: recommend python3-pil instead of depending on it, since
    upstreams says so.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Wed, 05 Aug 2020 22:04:25 -0400

python-mpv (0.5.1-1) unstable; urgency=medium

  [ Louis-Philippe Véronneau ]
  * New upstream version.
  * d/patches: drop 0001, as upstream fixed the testsuite.
  * d/tests: run the upstream testsuite as autopkgtest.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Mon, 20 Jul 2020 15:48:04 -0400

python-mpv (0.4.7-1) unstable; urgency=medium

  [ Louis-Philippe Véronneau ]
  * New upstream version.
  * d/patches: rebase 0001 and make it less crude.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Mon, 13 Jul 2020 14:18:56 -0400

python-mpv (0.4.6-1) unstable; urgency=medium

  [ Louis-Philippe Véronneau ]
  * New upstream version.
  * Add upstream's GPG key and verify it with uscan.
  * d/control: add new build-depends to run the testsuite.
  * d/patches: add patch to skip 4 failing tests.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Tue, 14 Apr 2020 01:54:44 -0400

python-mpv (0.4.5-1) unstable; urgency=medium

  [ Louis-Philippe Véronneau ]
  * Initial release. (Closes: #955190)

  [ Birger Schacht ]
  * Initial packaging work.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Sat, 28 Mar 2020 00:53:07 -0400
